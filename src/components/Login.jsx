import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

function Login() {
  const [email, setEmail] = useState("user@example.com");
  const [password, setPassword] = useState("password");
  const navigate = useNavigate();

  function handleSubmit() {
    const { VITE_SERVER_ADDRESS } = import.meta.env;
    if (email && password) {
      axios
        .post(`${VITE_SERVER_ADDRESS}/login`, {
          email,
          password,
        })
        .then((response) => {
          localStorage.setItem("TOKEN", response.data);
          toast("Logged successfully");
          navigate("/articles");
        })
        .catch((error) => {
          console.error(error);
          toast("Invalid credentials");
        });
    } else {
      toast("Please specify both email and password");
    }
  }

  return (
    <div id="container">
      <form>
        <h1>Connexion</h1>
 
        <label>Login</label>
        <input type="email" 
          placeholder="user@mail.com" 
          name="username" 
          value={email} 
          onChange={(e) => setEmail(e.target.value)} required />

        <label>Mot de passe</label>
        <input 
          type="password" 
          placeholder="********" 
          name="password" 
          value={password} 
          onChange={(e) => setPassword(e.target.value)} required />
        <button type="button" onClick={handleSubmit}>LOGIN</button>
      </form>
    </div>
  );
}

export default Login; 