import axios from "axios"
import { useEffect, useState } from "react"
import { useNavigate } from "react-router";


function ArticleList(){
    const { VITE_SERVER_ADDRESS } = import.meta.env;
    const [articles, setArticles] = useState([])
    const token = localStorage.getItem("TOKEN");
    const navigate = useNavigate();
    let source = axios.CancelToken.source();
    useEffect(()=>{
        axios.get(`${VITE_SERVER_ADDRESS}/gestiondestock/v1/articles/all`,{
            CancelToken: source.token,
            Headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        .then((response)=>{
            setArticles(response.data.content)
        })
        .catch((error) => {
            if (error?.code == "ERR_CANCELED") {
              // request canceled, do nothing
            } else if (error?.response?.status == 401) {
              toast("Unauthorized access");
              navigate("/login");
            } else {
              console.error(error);
            }
          });
    
        return function () {
          source.cancel("request canceled");
        };
    },[]);
    return(
        <div className="articleList">
            {articles.map(article => (
                <article key={article.idArticle}>
                <p id="nom">{article.nom}</p>
                <p id="code">{article.codeArticle}</p>
                <p id="prixHt" >{article.prixUnitaireHt}</p>
                <p id="prixTtc">{article.prixUnitaireTTC}</p>
                <p id="category">{article.category.nom}</p>
                <p id="entreprise">{article.entreprise.nom}</p>
            </article>
            ))}
        </div>
    )

}
export default ArticleList