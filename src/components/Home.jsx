import Login from "./Login";
import { useNavigate } from "react-router-dom";
function Home() {
  const navigate = useNavigate();
  return (
    <div className="Home">
      <Login />
    </div>
  );
}

export default Home;